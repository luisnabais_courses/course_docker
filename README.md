# Docker course
### Introduction
Welcome to my free Docker Course.
There are 3 levels included: 
- Fundamentals 
- Intermeditate
- Container Orchestration with Docker Swarm

### Course Program
#### Organization
This course is organized in the following manner:

#### Folders
* **cheat_sheet** - PDF file(s) with the most common commands, for easy reference
* **dockerfile** - Everything related to Dockerfile, Docker Compose and it's resources
* **resources** - External files used (ex: images)
* **slides** - Slides/Presentation, in OpenDocument Presentation and PowerPoint formats
* **vagrant** - Vagrant files for fast creation of VirtualBox machines, with Ansible provisioning
